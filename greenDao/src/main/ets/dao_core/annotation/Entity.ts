/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

import { Index, IndexEntity } from './Index'

class nameInDb {
    public readonly key: string = 'nameInDb';
    public value: any = '';

    constructor(value: string = '') {
        this.value = value;
    }
}

class indexes {
    public readonly key: string = 'index';
    public value: IndexEntity[];

    constructor(value: IndexEntity[] = [new IndexEntity()]) {
        this.value = value;
    }
}

class createInDb {
    public readonly key: string = 'createInDb';
    public value: boolean = true;

    constructor(value: boolean = true) {
        this.value = value;
    }
}

class schema {
    public readonly key: string = 'schema';
    public value = 'default';

    constructor(value: string = 'default') {
        this.value = value;
    }
}

class active {
    public readonly key: string = 'active';
    public value: boolean = false;

    constructor(value: boolean = false) {
        this.value = value;
    }
}

class generateConstructors {
    public readonly key: string = 'generateConstructors';
    public value: boolean = true;

    constructor(value: boolean = true) {
        this.value = value;
    }
}

class generateGettersSetters {
    public readonly key: string = 'generateGettersSetters';
    public value: boolean = true;

    constructor(value: boolean = true) {
        this.value = value;
    }
}

class protobuf {
    public readonly key: string = 'protobuf';
    public value: any;

    constructor(value: any = 'void' /*void.class*/
    ) {
        this.value = value;
    }
}


export class EntityE {
    nameInDbParam: nameInDb;
    indexesParam: indexes;
    createInDbParam: createInDb;
    schemaParam: schema;
    activeParam: active;
    generateConstructorsParam: generateConstructors;
    generateGettersSettersParam: generateGettersSetters;
    protobufParam: protobuf;

    constructor(
        nameInDbStr: string = '', indexesStr: IndexEntity[] = [new IndexEntity() /*Index[]= {}*/
    ],
        createInDbStr: boolean = true, schemaStr: string = 'default', activeStr: boolean = false,
        generateConstructorsStr: boolean = true, generateGettersSettersStr: boolean = true, protobufStr: any = 'void' /*void.class*/
    ) {
        this.nameInDbParam = new nameInDb(nameInDbStr);
        this.indexesParam = new indexes(indexesStr);
        this.createInDbParam = new createInDb(createInDbStr);
        this.schemaParam = new schema(schemaStr);
        this.activeParam = new active(activeStr);
        this.generateConstructorsParam = new generateConstructors(generateConstructorsStr);
        this.generateGettersSettersParam = new generateGettersSetters(generateGettersSettersStr);
        this.protobufParam = new protobuf(protobufStr);
    }
}


export function Entity(param: EntityE = new EntityE()): ClassDecorator {

    let nameInDbTmp: nameInDb = param.nameInDbParam;
    let indexesTmp: indexes = param.indexesParam;
    let createInDbTmp: createInDb = param.createInDbParam;
    let schemaTmp: schema = param.schemaParam;
    let activeTmp: active = param.activeParam;
    let generateConstructorsTmp: generateConstructors = param.generateConstructorsParam;
    let generateGettersSettersTmp: generateGettersSetters = param.generateGettersSettersParam;
    let protobufParam: protobuf = param.protobufParam;

    return (target) => {
        Reflect.defineMetadata(nameInDbTmp.key, nameInDbTmp.value, target); //'nameInDb'
        Reflect.defineMetadata(createInDbTmp.key, createInDbTmp.value, target); //'createInDb'
        Reflect.defineMetadata(schemaTmp.key, schemaTmp.value, target); //'schema'
        Reflect.defineMetadata(activeTmp.key, activeTmp.value, target); //'active'
        Reflect.defineMetadata(generateConstructorsTmp.key, generateConstructorsTmp.value, target); //'generateConstructors'
        Reflect.defineMetadata(generateGettersSettersTmp.key, generateGettersSettersTmp.value, target); //'generateGettersSetters'
        Reflect.defineMetadata(protobufParam.key, protobufParam.value, target); //'protobuf'

        let arr = indexesTmp.value;
        for (let index of arr) {
            let valueTmp = index.valueParam;
            let nameTmp = index.nameParam;
            let uniqueTmp = index.uniquesParam;

            Reflect.defineMetadata(valueTmp.key, valueTmp.value, target); //'value'
            Reflect.defineMetadata(nameTmp.key, nameTmp.value, target); //'name'
            Reflect.defineMetadata(uniqueTmp.key, uniqueTmp.value, target); //'unique'
        }
    }
}
