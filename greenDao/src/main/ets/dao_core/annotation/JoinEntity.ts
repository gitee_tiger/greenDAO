/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

/**
 * Defines *-to-* relation with join table
 */
class entity {
    public readonly key: string = 'entity';
    public value = {};

    constructor(value: any) {
        this.value = value;
    }
}

class sourceProperty {
    public readonly key: string = 'sourceProperty';
    public value = '';

    constructor(value: string) {
        this.value = value;
    }
}

class targetProperty {
    public readonly key: string = 'targetProperty';
    public value = '';

    constructor(value: string) {
        this.value = value;
    }
}

export function JoinEntity(entityStr: string, sourcePropertyStr: string, targetPropertyStr: string) {

    let entityTmp: entity = new entity(entityStr);
    let sourcePropertyTmp: sourceProperty = new sourceProperty(sourcePropertyStr);
    let targetPropertyTmp: targetProperty = new targetProperty(targetPropertyStr);

    return (target, propertyKey) => {
        Reflect.defineMetadata(entityTmp.key, entityTmp.value, target, propertyKey);
        Reflect.defineMetadata(sourcePropertyTmp.key, sourcePropertyTmp.value, target, propertyKey);
        Reflect.defineMetadata(targetPropertyTmp.key, targetPropertyTmp.value, target, propertyKey);
    }
}