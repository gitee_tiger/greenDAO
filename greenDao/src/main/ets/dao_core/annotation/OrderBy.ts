/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

/**
 * Specifies ordering of related collection of {@link ToMany} relation
 * E.g.: @OrderBy("name, age DESC") List collection;
 * If used as marker (@OrderBy List collection), then collection is ordered by primary key
 */
import 'reflect-metadata';

function OrderBy(v: string) {
    return (target, primeryKey) => {
        Reflect.defineMetadata('value', v, target, primeryKey);
    };
}