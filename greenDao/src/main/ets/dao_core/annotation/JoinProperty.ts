/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

/**
 * Defines name and referencedName properties for relations
 *
 * @see ToMany
 */
class name {
    public readonly key: string = 'name';
    public value: any = '';

    constructor(value: string) {
        this.value = value;
    }
}

class referencedName {
    public readonly key: string = 'referencedName';
    public value = '';

    constructor(value: string) {
        this.value = value;
    }
}

export class JoinPropertyEntity {
    nameParam: name;
    referencedNameParam: referencedName;

    constructor(nameStr: string = '', referencedNameStr: string = '') {
        this.nameParam = new name(nameStr);
        this.referencedNameParam = new referencedName(referencedNameStr);
    }
}

export function JoinProperty(param: JoinPropertyEntity = new JoinPropertyEntity()) {
    let nameTmp: name = param.nameParam;
    let referencedNameTmp: referencedName = param.referencedNameParam;

    return (target, propertyKey) => {
        Reflect.defineMetadata(nameTmp.key, nameTmp.value, target, propertyKey);
        Reflect.defineMetadata(referencedNameTmp.key, referencedNameTmp.value, target, propertyKey);
    }
}