/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

/**
 * Entity mapped to table "NOTE".
 */
import {Id} from '@ohos/greendao';
import {NotNull} from '@ohos/greendao';
import {Table, Columns} from '@ohos/greendao';
import {ColumnType} from '@ohos/greendao';

@Table('NOTE')
export  class Note {

  @Id()
  @Columns('ID', ColumnType.num)
  id: number;

  @NotNull()
  @Columns('TEXT', ColumnType.str)
  text: string;
  @Columns('COMMENT', ColumnType.str)
  comment: string;
  @Columns('DATE', ColumnType.str)
  date: Date;

  @Columns('TYPE', ColumnType.str)
  type: string;

  @Columns('MONEYS', ColumnType.real)
  moneys: number;

  //todo 类中必须在constructor中声明所有非静态变量，用于反射生成列
  constructor(id?: number, text?: string, comment?: string, date?: Date, types?: string,moneys?:number) {
    this.id = id;
    this.text = text;
    this.comment = comment;
    this.date = date;
    this.type = types;
    this.moneys=moneys;
  }
  getMoneys(): number {
    return  this.moneys;
  }

  setMoneys(moneys: number) {
    this.moneys = moneys;
  }
  getId(): number {
    return  this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getText(): string {
    return  this.text;
  }

  /** Not-null value; ensure this value is available before it is saved to the database. */
  setText(text: string) {
    this.text = text;
  }

  getComment(): string {
    return  this.comment;
  }

  setComment(comment: string) {
    this.comment = comment;
  }

  getDate(): Date {
    return  this.date;
  }

  setDate(date: Date) {
    this.date = date;
  }

  getType(): string{
    return this.type;
  }

  setType(types: string) {
    this.type = types;
  }

}
